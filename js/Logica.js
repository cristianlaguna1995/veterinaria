  // (function () {
  //   $(document).ready(()=>{
  //     const init = ()=>{
  //       loadBanner();
  //       loadFooter();
  //     }  
  //     const loadBanner = ()=>{
  //       $('.banner').load('./Banner.html');
  //     }  
  //     const loadFooter = ()=>{
  //       $('.footer').load('./Footer.html');
  //     }  
  //     init();
  //   });
  // }())

  (function () {
    $(document).ready(function () {
      function init() {
        loadBanner();
        loadFooter();
      }
  
      function loadBanner() {
        $('.banner').load('./Banner.html');  
      }
  
      function loadFooter() {
        $('.footer').load('./Footer.html');  
      }
  
      init();
    });
  }());